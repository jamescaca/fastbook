<?php

use Illuminate\Database\Seeder;
use App\Invoice;
use App\InvoiceItem;
use App\ItemType;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoices')->delete();
        DB::table('item_types')->delete();
        DB::table('invoice_items')->delete();

        ItemType::create(array(
            'item_type' => 'iron',
            'caption' => 'expensive',
            'deactivated' => 0,
            'price' => 12.0
        ));

        Invoice::create(array(
            'customer' => 'ma consulting',
            'description' => 'first job',
            'entry_date' => '2019-11-05',
            'from' => '2019-11-05 12:00:00',
            'to' => '2019-11-05 13:00:00',
            'hst' => 0.13,
            'job_num' => '#123123',
            'location' => 'Toronto (ON)',
            'phoneNo' => '6478921663',
            'subtotal' => 0,
            'total' => 0
        ));

        InvoiceItem::create(array(
            'ref_job_num' => '#123123',
            'item_code' => 1
        ));

    }
}
