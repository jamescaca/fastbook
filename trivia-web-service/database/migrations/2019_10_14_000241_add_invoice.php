<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_num')->unique();
            $table->date('entry_date');
            $table->time('from');
            $table->time('to');
            $table->string('customer');
            $table->string('phoneNo');
            $table->string('location');
            $table->string('description');
            $table->float('subtotal');
            $table->float('hst');
            $table->float('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('invoices');
    }
}
