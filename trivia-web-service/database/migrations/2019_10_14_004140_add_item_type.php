<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddItemType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('item_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_type');
            $table->string('caption');
            $table->tinyInteger('deactivated')->default('1');
            $table->float('price');
            $table->timestamps();
        });

        // Schema::table('invoice_item',function(Blueprint $table){
        //     $table->foreign('ref_job_num')
        //     ->references('job_num')
        //     ->on('invoices')
        //     ->onDelete('cascade');
        //     $table->foreign('item_code')
        //     ->references('id')
        //     ->on('item_type')
        //     ->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('item_type');
    }
}
