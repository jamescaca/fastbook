<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\InvoiceItem;
use App\Http\Resources\InvoiceItems as PlayerResource;
use App\Http\Resources\PlayerCollection;

class InvoiceItemsController extends Controller
{
    public function index()
    {
        return new PlayerCollection(InvoiceItem::all());
    }

    public function show($id)
    {
        return new PlayerResource(InvoiceItem::findOrFail($id));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        $invoice = InvoiceItem::create($request->all());

        return (new PlayerResource($invoice))
                ->response()
                ->setStatusCode(201);
    }

    public function delete($id)
    {
        $invoice = InvoiceItem::findOrFail($id);
        $invoice->delete();

        return response()->json(null, 204);
    }
}
