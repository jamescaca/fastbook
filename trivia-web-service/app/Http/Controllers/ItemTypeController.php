<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ItemType;
use App\Http\Resources\ItemTypes as PlayerResource;
use App\Http\Resources\PlayerCollection;

class ItemTypeController extends Controller
{
    public function index()
    {
        return new PlayerCollection(ItemType::all());
    }

    public function show($id)
    {
        return new PlayerResource(ItemType::findOrFail($id));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        $invoice = ItemType::create($request->all());

        return (new PlayerResource($invoice))
                ->response()
                ->setStatusCode(201);
    }

    public function delete($id)
    {
        $invoice = ItemType::findOrFail($id);
        $invoice->delete();

        return response()->json(null, 204);
    }
}
