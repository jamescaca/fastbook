<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB; 

use App\Invoice;
use App\datamodel;
use App\Http\Resources\Invoices as PlayerResource;
use App\Http\Resources\Ticket as TicketResource;
use App\Http\Resources\PlayerCollection;

class InvoiceController extends Controller
{
    public function index()
    {
        return new PlayerCollection(Invoice::all());
    }

    public function union(){
         $ticket = DB::table('invoices')
        ->select('invoices.id as invoice_id','invoice_items.item_code as code','item_types.price as price')//As many columns as you want
        ->join('invoice_items', 'invoices.job_num', '=', 'invoice_items.ref_job_num')
        ->join('item_types', 'invoice_items.item_code', '=', 'item_types.id')
        ->get();
        return [
            'data' => $ticket
        ];
    }

    public function show($id)
    {
        return new PlayerResource(Invoice::findOrFail($id));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        $invoice = Invoice::create($request->all());

        return (new PlayerResource($invoice))
                ->response()
                ->setStatusCode(201);
    }

    public function delete($id)
    {
        $invoice = Invoice::findOrFail($id);
        $invoice->delete();

        return response()->json(null, 204);
    }
}