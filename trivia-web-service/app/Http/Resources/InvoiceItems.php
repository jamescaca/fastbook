<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceItems extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ref_job_num' => $this->ref_job_num,
            'item_code' => $this->item_code,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
