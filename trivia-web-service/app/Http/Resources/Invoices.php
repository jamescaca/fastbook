<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class Invoices extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'job_num' => $this->job_num,
            'entry_date' => $this->entry_date,
            'from' => $this->from,
            'to' => $this->to,
            'customer' => $this->customer,
            'phoneNo' => $this->phoneNo,
            'location' => $this->location,
            'description' => $this->description,
            'subtotal' => $this->subtotal,
            'hst' => $this->hst,
            'total' => $this->total,
            'creted_at' => $this->creted_at,
            'updated_at' => $this->updated_at
        ];
    }
}
