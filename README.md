# Build a Basic CRUD App with Laravel and Angular

This example shows how to use Laravel and Angular to create a basic CRUD app for hosting trivia games, using Okta to handle authentication.

Please read (Article Placeholder) to see how this application was built.

**Prerequisites:** PHP, Node, [Okta developer account](https://developer.okta.com/)

> [Okta](https://developer.okta.com) has Authentication and User Management APIs that reduce development time with instant-on, scalable user infrastructure. Okta's intuitive API and expert support make it easy for developers to authenticate, manage, and secure users and roles in any application.

## Getting Started

Sign up for an [Okta developer account](https://developer.okta.com) and create a new application. Make note of the Client ID and Issuer values for your application.

Clone this project using the following commands:

```
git clone git@github.com:oktadeveloper/okta-php-trivia-angular.git
cd okta-php-trivia-angular
```

### Set up the Backend

Create the database and user for the project:

```
mysql -uroot -p
CREATE DATABASE trivia CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL on trivia.* to trivia@127.0.0.1 identified by 'trivia';
quit
```

Copy the example `.env` file:

```
cd trivia-web-service
cp .env.example .env
```

Put the database details in the `.env` file:

```
DB_DATABASE=trivia
DB_USERNAME=trivia
DB_PASSWORD=trivia
```

Edit `app/Http/Middleware/AuthenticateWithOkta.php` and replace the Okta credentials with your own.

Install the project dependencies, generate a project key, run the migrations and then start the server:

```
composer install
php artisan key:generate
php artisan migrate
php artisan serve
```

Loading [127.0.0.1:8000](127.0.0.1:8000) now should show the default Laravel page, and [127.0.0.1:8000/api/players](127.0.0.1:8000/api/players) should show you a 'Unauthorized' message. NOTE: if using a virtual machine and NAT, you might need to run the server as `php artisan serve --host 0.0.0.0` instead.

### Set up the Frontend

Edit the Okta configuration in `src/app/app.module.ts` to fill in your client ID and other application details. (Refer to the (Article Placeholder) for more details.) Then install the dependencies and run the server:

```
cd trivia-web-client-angular
npm install -g @angular/cli@^6.1
npm install
ng serve --open
```
NOTE: if using a virtual machine and NAT, you might need to run the server as `ng serve --open --host 0.0.0.0` instead.

Loading [127.0.0.1:4200](127.0.0.1:4200) now should show you the application.

## Help

Please post any questions as comments on the (Article Placeholder), or visit our [Okta Developer Forums](https://devforum.okta.com/). You can also email developers@okta.com if would like to create a support ticket.

## License

Apache 2.0, see [LICENSE](LICENSE).

# Process to continue

1.Build up new models

DB Migrations

https://laravel.com/docs/5.8/eloquent#introduction (work on creating models)

https://stackoverflow.com/questions/14449912/convention-table-names-with-underscore (controller, db table and eloquent model name conventions)

## Getting all model, controller and db migration ready

```
php artisan make:model InvoiceItems -mc;
php artisan make:model Invoice -mc;
php artisan make:model ItemType -mc;
```

https://laravel.com/docs/5.8/migrations#generating-migrations (add migration)

```
php artisan migrate;
```

### Test out new api config with postman (https://developer.okta.com/docs/guides/mfa/set-up-postman/)

### Setup OKTA Auth

### Start testing Basic Apis

### Build up front-end Angular

### Generate an Angular Component (https://angular.io/cli/generate)

```
ng generate component <home>;
```

adding a routing in app.module.ts

### Start DB Seeding

clear default bindings and re-build

```
composer dump autoload ;
php artisan make:seeder UsersTableSeeder;
```
#List All Available APIs

```
php artisan route:list
```

### Import Postman script and Test Out the APIs

### Play with enhanced complex data structure - joining tables

```
$articles = DB::table('articles')
            ->select('articles.id as articles_id', ..... )
            ->join('categories', 'articles.categories_id', '=', 'categories.id')
            ->join('users', 'articles.user_id', '=', 'user.id')
            ->get();
Response:json($articles);
```

### Make Resource Collection

To be notified.  Resrouce Collection has to work with Specific Model and Controllers.  It is not friendly to joint complex types.

```
php artisan make:resource Invoices --collection;
php artisan make:resource InvoiceItems --collection;
php artisan make:resource ItemTypes --collection;
```

Or

```
php artisan make:resource InvoiceCollection;
php artisan make:resource InvoiceItemCollection;
php artisan make:resource ItemTypeCollection;
```

## Start bulding front-end Angular Services

### Make the Web Service

Invoice Ticket & Install Meterial Design

```
ng generate service _services/ticket;
ng add @angular/material;
```

Define displaycolumns and make mat-table from template

### Fix Dependencies
```
npm install --save @angular/material @angular/cdk;
rm -rf node_modules;
npm install;
```

```
"rxjs": "6.3.3",
"rxjs-compat": "6.3.3",
"@angular/cdk": "^6.1.0",
"@angular/core": "^6.1.0",
"@angular/material": "^6.1.0",
```

import mattable module in app.module

### Create a Basic Mat Table

```
<div class="ticket-wrapper">
    <table mat-table [dataSource]="tickets" multiTemplateDataRows class="mat-elevation-z8 ticket-table">
      <ng-container matColumnDef="{{column}}" *ngFor="let column of displayedColumns;">
        <th mat-header-cell *matHeaderCellDef> {{renderingHeader(column)}} </th>
        <td mat-cell *matCellDef="let ticket"> {{ticket[renderingHeader(column)]}} 
          <!--<mat-checkbox *ngIf="column =='printable'" [(ngModel)]="ticket[0].projectPrintable"></mat-checkbox>-->
        </td>
      </ng-container>

    <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
    <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
    </table>
</div>
```  

### Flex Display Basis
```
.mat-table{
    width : 100%
}
.invoice-wrapper{
    .invoice-wrapper{
        .matheaderrow{
            display: flex;
            flex: 0 0 100%;
            .mat-header-cell{
                &.mat-column-ticket_invoice_id{
                    flex:0 0 20%;
                }
                &.mat-column-ticket_code{
                    flex:0 0 40%;
                }
                &.mat-column-ticket_price{
                    flex:0 0 40%;
                }
            }
            .mat-cell{
                &.mat-column-ticket_invoice_id{
                    flex:0 0 20%;
                }
                &.mat-column-ticket_code{
                    flex:0 0 40%;
                }
                &.mat-column-ticket_price{
                    flex:0 0 40%;
                }
            }
        }
    }
}
```