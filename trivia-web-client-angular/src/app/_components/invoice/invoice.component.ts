import { Component, OnInit, ViewChild } from '@angular/core';
import {TicketService} from '../../_services/ticket.service'
import { Ticket,MatCols } from 'src/app/_helper/DataModel';
import { MatSort } from '@angular/material';
import * as Rx from 'rxjs/Rx';
import * as _ from 'lodash';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
  private tickets:Ticket[];
  displayedColumns: string[] = ['ticket_invoice_id','ticket_code','ticket_price'];

  constructor(private ticketService : TicketService) { }

  ngOnInit() {
    this.getTickets();
  }

  getTickets(){
    this.ticketService.getTickets().subscribe(
      result => {
        this.tickets = result;
      }
    );
  }

  /*Rendering Header*/
  renderingHeader(key:string){
    for(var em in MatCols){
      if(em == key){return MatCols[em];}
    }
  }
  toLower(key:string){
    return key.toLowerCase();
  }
}