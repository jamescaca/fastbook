import { Injectable } from '@angular/core';
import { OktaAuthService } from '@okta/okta-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import {Ticket} from '../_helper/DataModel';
import {config} from '../_helper/config'
import { Observable } from 'rxjs';

const API_URL: string = config.baseUrl;//PHP artisan by default is 8000

@Injectable({
  providedIn: 'root'
})

export class TicketService{
  private accessToken;
  private headers;

  constructor(private oktaAuth: OktaAuthService, private http: Http) {
    this.init();
  }

  async init() {
    this.accessToken = await this.oktaAuth.getAccessToken();
    this.headers = new Headers({
        Authorization: 'Bearer ' + this.accessToken
    });
  }

  getTickets(): Observable<Ticket[]> {
    return this.http.get(API_URL + '/api/tickets',
        new RequestOptions({ headers: this.headers })
    )
    .map(res => {
        // let modifiedResult = res.json().data
        // modifiedResult = modifiedResult.map(function(player) {
        //     player.isUpdating = false;
        //     return player;
        // });
        return res.json().data;
    });
  }
}