//Load from service to invoice here
export class Invoice{
    id:number;
    job_num:string;
    entry_date:Date;
    from:Date;
    to:Date;
    customer:string;
    phoneNo:string;
    location:string;
    description:string;
    subtotal:number;
    hst:number;
    total:number;
    created_at:Date;
    updated_at:Date;
}

export class InvoiceItem{
    id:number;
    ref_job_num:string;
    item_code:string;
    created_at:Date;
    updated_at:Date;
}

export class ItemType{
    id:number;
    item_type:string;
    caption:string;
    deactivated:number;
    price:string;
    created_at:Date;
    updated_a:Date;
}

export class Ticket {
    invoice_id:number;
    code:string;
    price:number;
}

//Mat Table Columns
export enum MatCols{
    //Invoice Table
    inv_id = "id",
    inv_job_num = "job_num",
    inv_entry_date = "entry_date",
    inv_from = "from",
    inv_to = "to",
    inv_customer = "customer",
    inv_phoneNo = "phoneNo",
    inv_location = "location",
    inv_description = "description",
    inv_subtotal = "subtotal",
    inv_hst = "hst",
    inv_total = "total",
    inv_created_at = "created_at",
    inv_updated_at = "updated_at",
    
    //Invoice Item Table
    inv_item_id = 'id',
    inv_item_ref_job_num = 'ref_job_num',
    inv_item_item_code = 'item_code',
    inv_item_created_at = 'created_at',
    inv_item_updated_at = 'updated_at',

    //Item Type Table
    item_type_id = 'id',
    item_type_item_type = 'item_type',
    item_type_caption = 'caption',
    item_type_deactivated = 'deactivated',
    item_type_price = 'price',
    item_type_created_at = 'created_at',
    item_type_updated_at = 'updated_at',

    //Ticket Table
    ticket_invoice_id = 'invoice_id',
    ticket_code = 'code',
    ticket_price = 'price'
}